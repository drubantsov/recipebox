== README
 
The book of recipes, comfortable place for keeping your recipes,
and sharing them with your friends and relatives.

Steps for realization:

* Ruby version - 2.1.4;

* Rails version - 4.2.6;

* Using Gems: Devise, Searchkick, SimpleForm, Paperclip, Cocoon, Haml, Bootstrap..

* OS - Linux: Ubuntu Server 16.04;

* Software: Image Magic for Paperclip's gem and Elasticsearch for Searchkick.
  
* After 'Steps for realization' above, make git clone and:

* First of make 'rake db:migrate'

* Next step 'rake searchkick:reindex CLASS=Recipe' or you search won't be working.

* To turn on Admin instead of usuall user, should update boolean column Admin to the value 'true' for your defined user.