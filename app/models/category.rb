class Category < ActiveRecord::Base

 has_many :recipes
 validates :name, presence:true, uniqueness: true, length:{maximum:50}

end
