class Recipe < ActiveRecord::Base

 acts_as_votable
 
 searchkick

 has_attached_file :image, styles: { medium: "400x400>"}, default_url: "missing.png"
 validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

 validates :title, presence:true, uniqueness: true, length:{maximum:51}
 validates :description, presence:true, length:{maximum:351}

 belongs_to :user
 belongs_to :category
 
 has_many :comments, dependent: :destroy
 has_many :ingredients, dependent: :destroy
 has_many :directions, dependent: :destroy
 
 accepts_nested_attributes_for :ingredients, reject_if: proc {|attribute| attribute['name'].blank?},
 															               allow_destroy: true
 accepts_nested_attributes_for :directions, reject_if: proc {|attribute| attribute['step'].blank?}, 
 																						 allow_destroy: true

end
