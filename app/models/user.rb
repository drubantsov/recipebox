class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :recipes, dependent: :destroy
  has_many :comments, dependent: :destroy

  has_attached_file :avatar, styles: {medium: "200x200>", thumb: "70x70>" }, default_url: "missing_avatar.png", styles: {thumb: "70x70>" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 1.megabytes
 	
 	validates :email, uniqueness: true
 	validates :name, presence:true
 	
end

