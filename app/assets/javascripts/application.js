// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require bootstrap
//= require jquery_ujs
//= require turbolinks
//= require cocoon
//= require_tree .

/*jQuery(function($){
	$(".vote").click(function(){
		var current_item_a = $(this);
		$.ajax({
			url:'/recipyes/' + current_item_a.attr('recipe_id') + '/like',
			type: 'POST',
			data: {_method:'PUT'},
			success: function(result){
				$('.glyphicon.glyphicon-heart').update;
			}
		});
	});
});
*/

/*jQuery(function($){
	$("a.comment-delete").click(function(){
		var current_item_th = $(this).parents('table')[0];
		if(confirm('Really?')){
		$.ajax({
			url:'/recipyes/' + current_item_th.attr('recipe_id') + '/comments/' + current_item_th.attr('comment_id'),
			type: 'POST',
			data: {_method:'DELETE'},
			success: function(result){
				$(current_item_th).fadeOut(200);
				$console.log(result);
			}
			});
		};
	});
});*/