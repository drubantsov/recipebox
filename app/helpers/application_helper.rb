module ApplicationHelper

	def to_link(text)
		(text).gsub! /\s?(https?:\/\/[\S]+)\s?/,'<a href="\1">\1</a>'	
		text.html_safe
	end

end
